package com.jdnessity.restaurantapp.widgets;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;

public class CustomSearchView extends SearchView{

	public CustomSearchView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public CustomSearchView(Context context) {
		super(context);
	}
	
	@Override
	public void onActionViewCollapsed() {
		setQuery("", true);
		super.onActionViewCollapsed();
	}
	
	public void reset(){
		setOnQueryTextListener(null);
		setOnQueryTextFocusChangeListener(null);
		setQuery("", true);
	}
}
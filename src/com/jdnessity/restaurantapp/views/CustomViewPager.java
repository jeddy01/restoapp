package com.jdnessity.restaurantapp.views;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {

	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);

	}

	float mStartDragX;
	OnSwipeOutListener mListener;

	public void setOnSwipeOutListener(OnSwipeOutListener listener) {
		mListener = listener;
	}
	
	public void reset(){
		setCurrentItem(1, true);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (getCurrentItem() == getAdapter().getCount() - 1) {
			final int action = ev.getAction();
			float x = ev.getX();
			switch (action & MotionEventCompat.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				mStartDragX = x;
				break;
			case MotionEvent.ACTION_MOVE:
				break;
			case MotionEvent.ACTION_UP:
				if (x < mStartDragX) {
					if(mListener != null){
						mListener.onSwipeOutAtEnd();
					}
				} else {
					mStartDragX = 0;
				}
				break;
			}
		} else if(getCurrentItem() == 0){
			
			final int action = ev.getAction();
			float x = ev.getX();
			switch (action & MotionEventCompat.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				mStartDragX = x;
				break;
			case MotionEvent.ACTION_MOVE:
				break;
			case MotionEvent.ACTION_UP:
				if (x < mStartDragX) {
					
				} else {
					mStartDragX = 0;
					if(mListener != null){
						mListener.onSwipeOutAtBeginning();
					}
				}
				break;
			}
			
		} else {
			mStartDragX = 0;
		}
		return super.onTouchEvent(ev);
	}

	public interface OnSwipeOutListener {
		public void onSwipeOutAtEnd();
		public void onSwipeOutAtBeginning();
	}
}
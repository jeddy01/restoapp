package com.jdnessity.restaurantapp.listeners;

public interface OnViewCreatedListener{
	public void onViewCreated();	
}

package com.jdnessity.restaurantapp;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.jdnessity.restaurantapp.utilities.Installation;
import com.jdnessity.restaurantapp.utilities.Logger;

public class CoreApplication extends Application{

	private static CoreApplication instance;
	public static boolean DEBUGGABLE = false;
	public static String A_UUID;
	private static Toast toast;
	
	/** SharedPreference */
	public static SharedPreferences LINX_SHAREDPREF;
	public static final String PREFS_NAME = "RestoAppPref";
	
	public CoreApplication() {
		instance = this;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		// for printing logs
		DEBUGGABLE = (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;

		// for tracking app installations
		A_UUID = Installation.id(getContext());

		// SharedPreference
		LINX_SHAREDPREF = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
	}
	
	public static Context getContext() {
		return instance;
	}

	public static Application getApplication() {
		return instance;
	}
	
	public static void showToast(Context context, String message,
			int duration) {
		try {
			
			if (toast != null) {
				toast.setText(message);
			} else if (context != null) {
				toast = Toast.makeText(context, message, duration);
				toast.setGravity(Gravity.BOTTOM, 0, 0);
			}
			
			TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
			if (v != null){
				v.setGravity(Gravity.BOTTOM);
			}
			
			toast.show();
			
		} catch (Exception e) {
			Logger.e("showCenteredToast:", e);
		}
	}
}

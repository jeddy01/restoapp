/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jdnessity.restaurantapp.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.StrictMode;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * This class contains static utility methods.
 */
public class Utils {

	// Prevents instantiation.
	private Utils() {
	}

	/**
	 * Enables strict mode. This should only be called when debugging the
	 * application and is useful for finding some potential bugs or best
	 * practice violations.
	 */
	@TargetApi(11)
	public static void enableStrictMode() {
		// Strict mode is only available on gingerbread or later
		if (Utils.hasGingerbread()) {

			// Enable all thread strict mode policies
			StrictMode.ThreadPolicy.Builder threadPolicyBuilder = new StrictMode.ThreadPolicy.Builder()
					.detectAll().penaltyLog();

			// Enable all VM strict mode policies
			StrictMode.VmPolicy.Builder vmPolicyBuilder = new StrictMode.VmPolicy.Builder()
					.detectAll().penaltyLog();

			// Honeycomb introduced some additional strict mode features
			if (Utils.hasHoneycomb()) {
				// Flash screen when thread policy is violated
				threadPolicyBuilder.penaltyFlashScreen();
				// For each activity class, set an instance limit of 1. Any more
				// instances and
				// there could be a memory leak.
				// vmPolicyBuilder.setClassInstanceLimit(
				// ContactsListActivity.class, 1).setClassInstanceLimit(
				// ContactDetailActivity.class, 1);
			}

			// Use builders to enable strict mode policies
			StrictMode.setThreadPolicy(threadPolicyBuilder.build());
			StrictMode.setVmPolicy(vmPolicyBuilder.build());
		}
	}

	public static boolean hasFroyo() {
		// Can use static final constants like FROYO, declared in later versions
		// of the OS since they are inlined at compile time. This is guaranteed
		// behavior.
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	/**
	 * Uses static final constants to detect if the device's platform version is
	 * Gingerbread or later.
	 */
	public static boolean hasGingerbread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	/**
	 * Uses static final constants to detect if the device's platform version is
	 * Honeycomb or later.
	 */
	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	/**
	 * Uses static final constants to detect if the device's platform version is
	 * Honeycomb MR1 or later.
	 */
	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	/**
	 * Uses static final constants to detect if the device's platform version is
	 * Honeycomb MR2 or later.
	 */
	public static boolean hasHoneycombMR2() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2;
	}

	/**
	 * Uses static final constants to detect if the device's platform version is
	 * ICS or later.
	 */
	public static boolean hasICS() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
	}

	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

	public static void hideSoftKey(Context context, EditText edittext) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
	}

	public static void showSoftKey(Context context) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	public static class ScreenDimension {
		public int width;
		public int height;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static ScreenDimension getScreenDimension(Context context) {

		int measuredHeight = 0;
		int measuredWidth = 0;
		Point size = new Point();
		WindowManager w = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			w.getDefaultDisplay().getSize(size);
			measuredHeight = size.y;
			measuredWidth = size.x;
		} else {
			Display d = w.getDefaultDisplay();
			measuredHeight = d.getHeight();
			measuredWidth = d.getWidth();
		}

		ScreenDimension dimension = new ScreenDimension();
		dimension.height = measuredHeight;
		dimension.width = measuredWidth;

		return dimension;
	}

	public static final String KEY_COUNTRY = "country";
	public static final String KEY_TELEPHONE_CODE = "telephone_code";

	public static List<String> getSortedCountryNamesList() {
		Locale[] locales = Locale.getAvailableLocales();
		final List<String> countries = new ArrayList<String>();
		for (Locale locale : locales) {
			String country = locale.getDisplayCountry();
			if (country.trim().length() > 0 && !countries.contains(country)) {
				countries.add(country);
			}
		}

		Collections.sort(countries);
		return countries;
	}

	public static boolean containsCountry(
			List<HashMap<String, Object>> countries, String countryName) {
		for (int i = 0; i < countries.size(); i++) {
			if (countries.get(i).get(KEY_COUNTRY).equals(countryName)) {
				return true;
			}
		}
		return false;
	}

	public static int getCountryIndex(List<HashMap<String, Object>> countries,
			String countryName) {
		for (int i = 0; i < countries.size(); i++) {
			if (countries.get(i).get(KEY_COUNTRY).equals(countryName)) {
				return i;
			}
		}
		return -1;
	}

	public static int getCountryIndexFromCountryCode(
			List<HashMap<String, Object>> countries, String countryCode) {
		try {
			if (countries != null)
				for (int i = 0; i < countries.size(); i++) {
					if (countries.get(i) != null
							&& countries.get(i).get(KEY_TELEPHONE_CODE) != null) {
						Logger.f(
								"cc",
								"tel_code: "
										+ countries.get(i).get(
												KEY_TELEPHONE_CODE) + " == "
										+ countryCode);
						if (countries.get(i).get(KEY_TELEPHONE_CODE)
								.equals(countryCode)) {
							return i;
						}
					}
				}
		} catch (Exception e) {
			Logger.e("getCountryIndexFromCountryCode", e);
		}
		return -1;
	}

}

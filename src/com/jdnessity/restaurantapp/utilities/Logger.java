package com.jdnessity.restaurantapp.utilities;

import android.util.Log;

import com.jdnessity.restaurantapp.CoreApplication;

public class Logger {
	public final static String TAG = "RestaurantApp";
	private static String sAppPackage = "com.jdnessity.restaurantapp";

	public static void d(String tag, String message) {
		if (CoreApplication.DEBUGGABLE) {
			Log.d(tag, message);
		}
	}

	public static void i(String tag, String message) {
		if (CoreApplication.DEBUGGABLE) {
			Log.i(tag, message);
		}
	}

	public static void e(String tag, String message, Throwable tr) {
		if (CoreApplication.DEBUGGABLE) {
			Log.e(tag, message, tr);
		}
	}

	public static void e(String tag, String message) {
		if (CoreApplication.DEBUGGABLE) {
			Log.e(tag, message);
		}
	}

	public static void longInfo(String tag, String str) {
		if (str.length() > 4000) {
			d(tag, str.substring(0, 4000));
			longInfo(tag, str.substring(4000));
		} else
			d(tag, str);
	}

	public static void d(Class<?> class1, String message) {
		if (CoreApplication.DEBUGGABLE) {
			Log.d(TAG, class1.getSimpleName() + ": " + message);
		}
	}

	public static void e(Class<?> class1, String message, Throwable tr) {
		if (CoreApplication.DEBUGGABLE) {
			Log.e(TAG, class1.getSimpleName() + ": " + message, tr);
		}
	}

	public static void e(Class<?> class1, String message) {
		if (CoreApplication.DEBUGGABLE) {
			Log.e(TAG, class1.getSimpleName() + ": " + message);
		}
	}

	public static void longInfo(Class<?> class1, String str) {
		if (str != null && str.length() > 4000) {
			d(class1, str.substring(0, 4000));
			longInfo(class1, str.substring(4000));
		} else
			d(class1, str);
	}

	/**
	 * Returns the caller information from the stack trace
	 * 
	 * @return [Class@Method:line] string or empty
	 */
	private static String caller() {
		try {
			StackTraceElement[] stack = Thread.currentThread().getStackTrace();
			for (StackTraceElement elem : stack) {
				String c = elem.getClassName();
				if (c.startsWith(sAppPackage)
						&& !c.equals(Logger.class.getName())) {
					return "[" + elem.getClassName().replace(sAppPackage, "")
							+ " @ " + elem.getMethodName() + ": "
							+ elem.getLineNumber() + "] ";
				}
			}
		} catch (Exception e) {
		}
		return "";
	}

	/**
	 * Format the log
	 * 
	 * @param msg
	 * @return
	 */
	private static String f(String msg) {
		return caller() + msg;
	}

	public static void d(String msg) {
		if (CoreApplication.DEBUGGABLE) {
			Log.d(TAG, f(msg));
		}
	}

	public static void f(String tag, String msg) {
		if (CoreApplication.DEBUGGABLE) {
			Log.d(tag, f(msg));
		}
	}

	public static void d(String msg, Exception e) {
		if (CoreApplication.DEBUGGABLE) {
			Log.d(TAG, f(msg), e);
		}
	}

	public static void i(String msg) {
		if (CoreApplication.DEBUGGABLE) {
			Log.i(TAG, f(msg));
		}
	}

	public static void i(String msg, Exception e) {
		if (CoreApplication.DEBUGGABLE) {
			Log.i(TAG, f(msg), e);
		}
	}

	public static void e(String msg) {
		Log.e(TAG, f(msg));
	}

	public static void e(String msg, Exception e) {
		Log.e(TAG, f(msg), e);
	}

	public static void w(String msg) {
		Log.w(TAG, f(msg));
	}

	public static void w(String msg, Exception e) {
		Log.w(TAG, f(msg), e);
	}

}

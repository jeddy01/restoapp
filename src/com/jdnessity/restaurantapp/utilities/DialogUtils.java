package com.jdnessity.restaurantapp.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jdnessity.restaurantapp.R;

public class DialogUtils {

	public static void showErrorDialog(Context context, String title,
			String message) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			if (!TextUtils.isEmpty(title))
				builder.setTitle(title);
			if (!TextUtils.isEmpty(message))
				builder.setMessage(message);
			builder.setNeutralButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			final AlertDialog dialog = builder.create();
			dialog.show();
		} catch (Exception e) {
			Logger.e("Exception:", e);
		}
	}

	public static void showErrorDialogWithTwoOptions(final Context context,
			String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		if (!TextUtils.isEmpty(title))
			builder.setTitle(title);
		if (!TextUtils.isEmpty(message))
			builder.setMessage(message);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.setNegativeButton("Enter Code",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO
						// SharedPreferences sharedPreferences =
						// PreferenceManager
						// .getDefaultSharedPreferences(context);
						// String userid = sharedPreferences.getString(
						// CoreApplication.PREF_KEY_USERID, null);
						// String min = sharedPreferences.getString(
						// CoreApplication.PREF_KEY_MIN, null);
						// if (!TextUtils.isEmpty(userid)
						// && !TextUtils.isEmpty(min)) {
						// Intent i = new Intent(context,
						// CodeVerifyActivity.class);
						// context.startActivity(i);
						// }
						dialog.dismiss();
					}
				});
		final AlertDialog dialog = builder.create();
		dialog.show();
	}

	public static void showDialogWithPreference(Context context, String title,
			String message, final Runnable onPreferenceChecked, ViewGroup parent) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			if (!TextUtils.isEmpty(title))
				builder.setTitle(title);

			LayoutInflater layoutInflater = LayoutInflater.from(context);
			LinearLayout contentLayout = (LinearLayout) layoutInflater.inflate(
					R.layout.custom_dialog_contentview, parent, false);

			TextView customDialogTextView = (TextView) contentLayout
					.findViewById(R.id.customDialogTextView);
			customDialogTextView.setText(message);
			customDialogTextView.setVerticalScrollBarEnabled(true);
			customDialogTextView
					.setMovementMethod(new ScrollingMovementMethod());

			final CheckBox checkBox = (CheckBox) contentLayout
					.findViewById(R.id.customDialogCheckBox);
			checkBox.setText("Do not show this dialog again.");
			checkBox.setChecked(true);//Default

			builder.setView(contentLayout);
			builder.setCancelable(false);
			builder.setOnKeyListener(new DialogInterface.OnKeyListener() {

				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_SEARCH
							&& event.getRepeatCount() == 0) {
						return true;
					} else if (keyCode == KeyEvent.KEYCODE_MENU
							&& event.getRepeatCount() == 0) {
						return true;
					}
					return false;
				}
			});
			builder.setNeutralButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			final AlertDialog dialog = builder.create();

			dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					if (onPreferenceChecked != null) {

						// Execute runnable if and only if the checkbox is
						// checked
						if (checkBox.isChecked()) {
							onPreferenceChecked.run();
						}
					}
				}
			});

			dialog.show();
		} catch (Exception e) {
			Logger.e("Exception:", e);
		}
	}
}

package com.jdnessity.restaurantapp.utilities;

import java.util.concurrent.Executor;

import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;

/**
 * Uses level 11 APIs when possible to use parallel/serial executors and falls
 * back to standard execution if API level is below 11.
 * 
 * https://gist.github.com/greenrobot/2773896
 * 
 */
public class AsyncTaskExecutionHelper {

	static class HoneycombExecutionHelper {
		@SafeVarargs
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		public static <P> void execute(AsyncTask<P, ?, ?> asyncTask,
				boolean parallel, P... params) {
			Executor executor = parallel ? AsyncTask.THREAD_POOL_EXECUTOR
					: AsyncTask.SERIAL_EXECUTOR;
			asyncTask.executeOnExecutor(executor, params);
		}
	}

	@SafeVarargs
	public static <P> void executeParallel(AsyncTask<P, ?, ?> asyncTask,
			P... params) {
		execute(asyncTask, true, params);
	}

	@SafeVarargs
	public static <P> void executeSerial(AsyncTask<P, ?, ?> asyncTask,
			P... params) {
		execute(asyncTask, false, params);
	}

	@SafeVarargs
	private static <P> void execute(AsyncTask<P, ?, ?> asyncTask,
			boolean parallel, P... params) {
		if (Utils.hasHoneycomb()) {
			HoneycombExecutionHelper.execute(asyncTask, parallel, params);
		} else {
			asyncTask.execute(params);
		}
	}
}

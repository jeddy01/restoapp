package com.jdnessity.restaurantapp.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jdnessity.restaurantapp.listeners.OnViewCreatedListener;

public class RootFragment extends Fragment{

	public static final String TAG = "RootFragment";
	
	protected String fragmentTitle;
	private OnViewCreatedListener onViewCreatedListener;
	
	/**
	 * View Life Cycle Methods
	 */
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		
		if(onViewCreatedListener != null){
			onViewCreatedListener.onViewCreated();
		}
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}
	
	/**
	 * Custom Methods
	 */
	
	public void setOnViewCreatedListener(OnViewCreatedListener onViewCreatedListener){
		this.onViewCreatedListener = onViewCreatedListener;
	}
}

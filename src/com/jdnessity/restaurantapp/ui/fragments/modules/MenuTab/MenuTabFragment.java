package com.jdnessity.restaurantapp.ui.fragments.modules.MenuTab;

import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.ui.fragments.RootFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MenuTabFragment extends RootFragment{

	public static final String TAG = "MenuTabFragment";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.fragmentTitle = TAG;
		setHasOptionsMenu(false);
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_menu_tab, container, false); 
		return view; 
	}
	
}

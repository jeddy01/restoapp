package com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab.objects;

import android.graphics.Bitmap;

public class OrderRecommendationData {
	public String promoCode;
	public Bitmap promoImage;
}

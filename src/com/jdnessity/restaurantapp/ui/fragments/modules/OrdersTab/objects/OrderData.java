package com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab.objects;

public class OrderData {

	public long id;
	public double quantity;
	public String itemTitle;
	public Currency currency;
	public double price;
	
	public static enum Currency{
		PH_PESO,
		US_DOLLAR,
		HK_DOLLAR,
		SG_DOLLAR,
		JP_YEN
	}
}

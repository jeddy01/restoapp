package com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jdnessity.restaurantapp.CoreApplication;
import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.ui.adapters.OrderRecommendationsPagerAdapter;
import com.jdnessity.restaurantapp.ui.fragments.RootFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab.objects.OrderData;
import com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab.objects.OrderData.Currency;
import com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab.objects.OrderRecommendationData;
import com.jdnessity.restaurantapp.utilities.Logger;
import com.jdnessity.restaurantapp.views.CustomViewPager;
import com.jdnessity.restaurantapp.views.CustomViewPager.OnSwipeOutListener;

public class OrdersTabFragment extends RootFragment implements View.OnClickListener{

	public static final String TAG = "OrdersTabFragment";
	
	private TableLayout tlReviewDetailItems;
	private CustomViewPager orderRecommendationsViewPager;
	private LinearLayout pagerIndicatorContainer;
    private Button btnViewRecommendedItem;
    
	/**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private OrderRecommendationsPagerAdapter mPagerAdapter;
    private ArrayList<ImageButton> pagerIndicators;
    private int currentSelectedPage;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.fragmentTitle = TAG;
		setHasOptionsMenu(false);
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_orders_tab, container, false);
		tlReviewDetailItems = (TableLayout) view.findViewById(R.id.tlReviewDetailItems);
		orderRecommendationsViewPager = (CustomViewPager) view.findViewById(R.id.orderRecommendationsViewPager);
		pagerIndicatorContainer = (LinearLayout) view.findViewById(R.id.pagerIndicatorContainer);
		btnViewRecommendedItem = (Button) view.findViewById(R.id.btnViewRecommendedItem);
		btnViewRecommendedItem.setOnClickListener(this);
		return view; 
	}
	
	private void generateOrderDetailItems(ViewGroup parent){
		
		Logger.d(TAG, "WHAT1");
		
		if(parent != null){
			parent.removeAllViews();
		}
		
		LayoutInflater inflater = null;
		//Resources resources = getResources();
		
		ArrayList<OrderData> orderDataList = generateDummyOrderDataList();
		
		for(int i=0; i<orderDataList.size(); i++){
			
			inflater = LayoutInflater.from(getActivity());
			
			final View childView = inflater.inflate(R.layout.layout_orders_item_table_row, parent, false);
			final OrderData loadedData = orderDataList.get(i);
			
			if(loadedData != null){
				
				TextView tvQtyDetail = (TextView) childView.findViewById(R.id.tvQtyDetail);
				TextView tvItemDetail = (TextView) childView.findViewById(R.id.tvItemDetail);
				TextView tvPriceDetail = (TextView) childView.findViewById(R.id.tvPriceDetail);
				Button btnDeleteItem  = (Button) childView.findViewById(R.id.btnDeleteItem);
				
				tvQtyDetail.setText(String.valueOf(loadedData.quantity));
				tvItemDetail.setText(loadedData.itemTitle);
				
				String prefixCurrency = "";
				switch(loadedData.currency){
					case PH_PESO:
						prefixCurrency = "Php ";
						break;
					default:
						prefixCurrency = "Price ";
						break;
				}
				tvPriceDetail.setText(prefixCurrency + String.format("%.2f", loadedData.price));
				btnDeleteItem.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						CoreApplication.showToast(getActivity(), "Delete " + loadedData.itemTitle, Toast.LENGTH_LONG);
					}
				});
				
				parent.addView(childView);
			}
		}
	}
	
	private ArrayList<OrderData> generateDummyOrderDataList(){
		ArrayList<OrderData> orderDataList = new ArrayList<OrderData>();
		
		for(int i=0; i<4; i++){
			
			OrderData data = new OrderData();
			data.id = 1000 + i;
			data.currency = Currency.PH_PESO;
			
			switch(i){
			case 0:
				data.quantity = 1;
				data.itemTitle = "Mango Juice";
				data.price = 60.00;
				break;
			case 1:
				data.quantity = 8;
				data.itemTitle = "Orange Juice";
				data.price = 60.00;
				break;
			case 2:
				data.quantity = 32;
				data.itemTitle = "Beef Teriyaki";
				data.price = 120.00;
				break;
			case 3:
				data.quantity = 10;
				data.price = 240.00;
				data.itemTitle = "Salmon Steak";
				break;
			}
			
			orderDataList.add(data);
		}
		
		return orderDataList;
	}
	
	private void setupRecommendationsPagerView(){
		
		Logger.d(TAG, "WHAT2");
		
		mPagerAdapter = new OrderRecommendationsPagerAdapter(getChildFragmentManager());
		orderRecommendationsViewPager.setOnSwipeOutListener(new OnSwipeOutListener() {
			@Override
			public void onSwipeOutAtEnd() {
				//Do something
			}
			
			@Override
			public void onSwipeOutAtBeginning() {
				//Do something
			}
		});
        
        OnPageChangeListener onPageChangeListener = new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				Logger.d("Page #" + arg0);
				setSelectedPagerIndicator(arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				
			}
		};
        
		orderRecommendationsViewPager.setOnPageChangeListener(onPageChangeListener);
		ArrayList<OrdersRecommendationPageFragment> orderRecommendedItems = new ArrayList<OrdersRecommendationPageFragment>();
        
        for(int i=0; i<5; i++){
        	
        	OrdersRecommendationPageFragment fragmentItem = new OrdersRecommendationPageFragment();
        	OrderRecommendationData orderRecommendationData = null;
        	switch(i){
        		case 0:
		        	orderRecommendationData = new OrderRecommendationData();
		        	orderRecommendationData.promoCode = "AA123AFBASDL";
		        	Bitmap icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_contact_picture_holo_light);
		        	orderRecommendationData.promoImage = Bitmap.createBitmap(icon);
		        	break;
        		case 1:
        			orderRecommendationData = new OrderRecommendationData();
		        	orderRecommendationData.promoCode = "444444444444";
		        	icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_launcher);
		        	orderRecommendationData.promoImage = Bitmap.createBitmap(icon);
        			break;
        		case 2:
        			orderRecommendationData = new OrderRecommendationData();
		        	orderRecommendationData.promoCode = "333333333333";
		        	icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_action_search);
		        	orderRecommendationData.promoImage = Bitmap.createBitmap(icon);
        			break;
        		case 3:
        			orderRecommendationData = new OrderRecommendationData();
		        	orderRecommendationData.promoCode = "111111111111";
		        	icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_action_overflow);
		        	orderRecommendationData.promoImage = Bitmap.createBitmap(icon);
        			break;
        		case 4:
        			orderRecommendationData = new OrderRecommendationData();
		        	orderRecommendationData.promoCode = "222222222222";
		        	icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_contact_picture_holo_light);
		        	orderRecommendationData.promoImage = Bitmap.createBitmap(icon);
        			break;
        	}
        	fragmentItem.setOrderRecommendationData(orderRecommendationData);
        	orderRecommendedItems.add(fragmentItem);
        }

        mPagerAdapter.setItems(orderRecommendedItems);
        orderRecommendationsViewPager.setAdapter(mPagerAdapter);
        setupPagerIndicator(mPagerAdapter);
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		generateOrderDetailItems(tlReviewDetailItems);
		setupRecommendationsPagerView();
	}
	
	private void setupPagerIndicator(OrderRecommendationsPagerAdapter adapter) {
		pagerIndicators = new ArrayList<ImageButton>();

		LayoutInflater inflater = LayoutInflater.from(getActivity());
		for (int i = 0; i < adapter.getCount(); i++) {
			LinearLayout root = (LinearLayout) inflater.inflate(R.layout.layout_page_indicator, pagerIndicatorContainer, true);
			ImageButton button = (ImageButton) root.findViewById(R.id.btn);
			button.setId(i);
			pagerIndicators.add(button);
		}
		setSelectedPagerIndicator(0);
	}

	private void setSelectedPagerIndicator(int btnIndex) {
		currentSelectedPage = btnIndex;
		for (int i = 0; i < pagerIndicators.size(); i++) {
			ImageButton btn = pagerIndicators.get(i);
			if (i == btnIndex) {
				btn.setImageResource(R.drawable.rounded_cell_dark);
			} else {
				btn.setImageResource(R.drawable.rounded_cell_light);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.btnViewRecommendedItem:
				OrdersRecommendationPageFragment selectedRecommendedItem = mPagerAdapter.getItem(currentSelectedPage);
				if(selectedRecommendedItem != null){
					OrderRecommendationData orderRecommendationData = selectedRecommendedItem.getOrderRecommendation();
					if(orderRecommendationData != null){
						CoreApplication.showToast(CoreApplication.getContext(), "Recommended Item Code : " + orderRecommendationData.promoCode, Toast.LENGTH_LONG);
					}
				}
				break;
			default:
				Logger.d(TAG, "Cannot find Clicked View..");
				break;
		}
	}
	
}

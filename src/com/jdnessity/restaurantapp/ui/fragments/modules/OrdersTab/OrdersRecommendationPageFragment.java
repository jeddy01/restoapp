package com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.ui.fragments.RootFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab.objects.OrderRecommendationData;

public class OrdersRecommendationPageFragment extends RootFragment{

	public final static String TAG = "OrdersRecommendationPageFragment";
	
	private ImageView featuredImgView;
	private OrderRecommendationData orderRecommendationData;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_orders_tab_recommendation_screen_slide_page, container, false);
        
        featuredImgView = (ImageView) rootView.findViewById(R.id.featuredImgView);
        if(this.orderRecommendationData != null && this.orderRecommendationData.promoImage != null){
        	featuredImgView.setImageBitmap(this.orderRecommendationData.promoImage);
        }
        
        return rootView;
    }

	public OrderRecommendationData getOrderRecommendation() {
		return orderRecommendationData;
	}

	public void setOrderRecommendationData(OrderRecommendationData orderRecommendationData) {
		this.orderRecommendationData = orderRecommendationData;
	}
}
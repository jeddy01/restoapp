package com.jdnessity.restaurantapp.ui.fragments.modules.OtherServicesTab.objects;

import android.graphics.Bitmap;

public class OtherServicesData {

	public long id;
	public String headerText;
	public String detailText;
	public Bitmap image;
	
}

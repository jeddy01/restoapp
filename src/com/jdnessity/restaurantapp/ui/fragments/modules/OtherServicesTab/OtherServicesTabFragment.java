package com.jdnessity.restaurantapp.ui.fragments.modules.OtherServicesTab;

import java.util.ArrayList;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.ui.adapters.OtherServicesListAdapter;
import com.jdnessity.restaurantapp.ui.fragments.RootFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.OtherServicesTab.objects.OtherServicesData;

public class OtherServicesTabFragment extends RootFragment{

public static final String TAG = "OtherServicesTabFragment";
	
	private ListView otherServicesListView;
	private OtherServicesListAdapter otherServicesAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.fragmentTitle = TAG;
		setHasOptionsMenu(false);
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_other_services_tab, container, false);
		otherServicesListView = (ListView) view.findViewById(android.R.id.list);
		return view; 
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		ArrayList<OtherServicesData> otherServicesDataList = generateDummyListData();
		otherServicesAdapter = new OtherServicesListAdapter(getActivity(), otherServicesDataList);
		otherServicesListView.setAdapter(otherServicesAdapter);
		
	}
	
	private ArrayList<OtherServicesData> generateDummyListData(){
		ArrayList<OtherServicesData> dataList = new ArrayList<OtherServicesData>();
		
		for(int i=0; i<10; i++){
			OtherServicesData data = new OtherServicesData();
			data.id = 1000 + i;
			data.headerText = "Party Reservation #" + i;
			data.detailText = "A few bits about this service #" + i;
			data.image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_contact_picture_holo_light);
			
			dataList.add(data);
		}
		
		return dataList;
	}
	
}

package com.jdnessity.restaurantapp.ui.fragments.modules.FeedbackTab.objects;

public class FeedbackRatingData {

	public String title;
	public String minTitle;
	public String maxTitle;
	public int maxValue;
	public int value; 
	
}

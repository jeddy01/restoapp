package com.jdnessity.restaurantapp.ui.fragments.modules.FeedbackTab;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.ui.fragments.RootFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.FeedbackTab.objects.FeedbackRatingData;
import com.jdnessity.restaurantapp.utilities.Logger;

public class FeedbackTabFragment extends RootFragment implements View.OnClickListener, SeekBar.OnSeekBarChangeListener{

	public static final String TAG = "FeedbackTabFragment";
	
	private EditText etFeedback;
	private Button btnSubmitFeedback;
	private LinearLayout feedbackRatingsContainer;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.fragmentTitle = TAG;
		setHasOptionsMenu(false);
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_feedback_tab, container, false);
		
		this.etFeedback = (EditText) view.findViewById(R.id.etFeedback);
		this.btnSubmitFeedback = (Button) view.findViewById(R.id.btnSubmitFeedback);
		this.btnSubmitFeedback.setOnClickListener(this);
		this.feedbackRatingsContainer = (LinearLayout) view.findViewById(R.id.feedbackRatingsContainer);
		
		return view; 
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		generateRatingsLayout(feedbackRatingsContainer);
	}
	
	private void generateRatingsLayout(ViewGroup parent){
		
		if(parent != null){
			parent.removeAllViews();
		}
		
		LayoutInflater inflater = null;
		Resources resources = getResources();
		
		String[] feedbackRatingHeaders = resources.getStringArray(R.array.rating_menu_headers);
		
		for(int i=0; i<feedbackRatingHeaders.length; i++){
			
			inflater = LayoutInflater.from(getActivity());
			View childView = inflater.inflate(R.layout.layout_feedback_rating_list_item, parent, false);
			
			FeedbackRatingData data = new FeedbackRatingData();
			
			data.title = feedbackRatingHeaders[i];
			data.maxTitle = resources.getString(R.string.very_happy);
			data.minTitle = resources.getString(R.string.unhappy);
			data.maxValue = 5;
			data.value = 0;
			
			TextView tvFeedbackRatingItemHeader = (TextView) childView.findViewById(R.id.tvFeedbackRatingItemHeader);
			TextView tvRatingMinLabel = (TextView) childView.findViewById(R.id.tvRatingMinLabel);
			TextView tvRatingMaxLabel = (TextView) childView.findViewById(R.id.tvRatingMaxLabel);
			
			SeekBar sbRating = (SeekBar) childView.findViewById(R.id.sbRating);
			sbRating.setOnSeekBarChangeListener(this);
			sbRating.setProgress(data.value);
			sbRating.setTag(data);
			
			tvFeedbackRatingItemHeader.setText(data.title);
			sbRating.setMax(data.maxValue);
			tvRatingMinLabel.setText(data.minTitle);
			tvRatingMaxLabel.setText(data.maxTitle);
			
			childView.setTag(data);
			parent.addView(childView);
		}
		
	}

	private void sendFeedback(){
		String strFeedback = etFeedback.getText().toString();
		//TODO: Handle logic here for sending feedback
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.btnSubmitFeedback:
				sendFeedback();
				break;
			default:
				Logger.d(TAG, "Cannot find Clicked View..");
				break;
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		
		Logger.d("OnProgressChanged", "Progress: " + progress);
		
		Object tag = seekBar.getTag();
		if(tag != null){
			FeedbackRatingData data = (FeedbackRatingData) tag;
			data.value = progress;
		}
		
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		
	}
}

package com.jdnessity.restaurantapp.ui.fragments.modules.FeaturedTab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.ui.fragments.RootFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.FeaturedTab.objects.FeaturedData;
import com.jdnessity.restaurantapp.utilities.Logger;

public class FeaturedItemPageFragment extends RootFragment{

	public final static String TAG = "FeaturedItemPageFragment";
	
	private ImageView featuredImgView;
	private FeaturedData featuredData;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_featured_tab_screen_slide_page, container, false);
        
        featuredImgView = (ImageView) rootView.findViewById(R.id.featuredImgView);
        if(this.featuredData != null && this.featuredData.promoImage != null){
        	Logger.d("TEST settingImageBitmap: " + this.featuredData.promoImage);
        	featuredImgView.setImageBitmap(this.featuredData.promoImage);
        }
        
        
        return rootView;
    }

	public FeaturedData getFeaturedData() {
		return featuredData;
	}

	public void setFeaturedData(FeaturedData featuredData) {
		this.featuredData = featuredData;
	}
}
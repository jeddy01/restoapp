package com.jdnessity.restaurantapp.ui.fragments.modules.FeaturedTab;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jdnessity.restaurantapp.CoreApplication;
import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.ui.adapters.FeaturedItemPagerAdapter;
import com.jdnessity.restaurantapp.ui.fragments.RootFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.FeaturedTab.objects.FeaturedData;
import com.jdnessity.restaurantapp.utilities.Logger;
import com.jdnessity.restaurantapp.views.CustomViewPager;
import com.jdnessity.restaurantapp.views.CustomViewPager.OnSwipeOutListener;

public class FeaturedTabFragment extends RootFragment implements View.OnClickListener{

	public static final String TAG = "FeaturedTabFragment";
	
	//The number of pages (wizard steps) to show in this demo.

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private CustomViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private FeaturedItemPagerAdapter mPagerAdapter;

    private ArrayList<ImageButton> pagerIndicators;
    private ViewGroup pagerIndicatorContainer;
    
    private Button btnAvailFeaturedItem;
    private int currentSelectedPage;
    
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.fragmentTitle = TAG;
		setHasOptionsMenu(false);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_featured_tab, container, false);
		
		 // Instantiate a ViewPager and a PagerAdapter.
        mPager = (CustomViewPager) view.findViewById(R.id.featuredTabPager);
        pagerIndicatorContainer = (LinearLayout) view.findViewById(R.id.pagerIndicatorContainer);
        btnAvailFeaturedItem = (Button) view.findViewById(R.id.btnAvailFeaturedItem);
        btnAvailFeaturedItem.setOnClickListener(this);
		return view; 
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		initializePager();
	}
	
	private void initializePager(){
		mPagerAdapter = new FeaturedItemPagerAdapter(getChildFragmentManager());
        mPager.setOnSwipeOutListener(new OnSwipeOutListener() {
			@Override
			public void onSwipeOutAtEnd() {
				//Do something
			}
			
			@Override
			public void onSwipeOutAtBeginning() {
				//Do something
			}
		});
        
        OnPageChangeListener onPageChangeListener = new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				Logger.d("Page #" + arg0);
				setSelectedPagerIndicator(arg0);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		};
        
        mPager.setOnPageChangeListener(onPageChangeListener);
        
        ArrayList<FeaturedItemPageFragment> promoItems = new ArrayList<FeaturedItemPageFragment>();
        
        for(int i=0; i<5; i++){
        	FeaturedItemPageFragment fragmentItem = new FeaturedItemPageFragment();
        	
        	FeaturedData featuredData = null;
        	switch(i){
        		case 0:
		        	featuredData = new FeaturedData();
		        	featuredData.promoCode = "AA123AFBASDL";
		        	Bitmap icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_contact_picture_holo_light);
		        	featuredData.promoImage = Bitmap.createBitmap(icon);
		        	break;
        		case 1:
        			featuredData = new FeaturedData();
		        	featuredData.promoCode = "444444444444";
		        	icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_launcher);
		        	featuredData.promoImage = Bitmap.createBitmap(icon);
        			break;
        		case 2:
        			featuredData = new FeaturedData();
		        	featuredData.promoCode = "333333333333";
		        	icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_action_search);
		        	featuredData.promoImage = Bitmap.createBitmap(icon);
        			break;
        		case 3:
        			featuredData = new FeaturedData();
		        	featuredData.promoCode = "111111111111";
		        	icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_action_overflow);
		        	featuredData.promoImage = Bitmap.createBitmap(icon); 
        			break;
        		case 4:
        			featuredData = new FeaturedData();
		        	featuredData.promoCode = "222222222222";
		        	icon = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.ic_contact_picture_holo_light);
		        	featuredData.promoImage = Bitmap.createBitmap(icon);
        			break;
        	}
        	
        	fragmentItem.setFeaturedData(featuredData);
        	promoItems.add(fragmentItem);
        }

        mPagerAdapter.setItems(promoItems);
        mPager.setAdapter(mPagerAdapter);
        setupPagerIndicator(mPagerAdapter);
	}
	
	private void setupPagerIndicator(FeaturedItemPagerAdapter adapter) {
		pagerIndicators = new ArrayList<ImageButton>();

		LayoutInflater inflater = LayoutInflater.from(getActivity());
		for (int i = 0; i < adapter.getCount(); i++) {
			LinearLayout root = (LinearLayout) inflater.inflate(R.layout.layout_page_indicator, pagerIndicatorContainer, true);
			ImageButton button = (ImageButton) root.findViewById(R.id.btn);
			button.setId(i);
			pagerIndicators.add(button);
		}
		setSelectedPagerIndicator(0);
	}

	private void setSelectedPagerIndicator(int btnIndex) {
		
		currentSelectedPage = btnIndex;
		
		for (int i = 0; i < pagerIndicators.size(); i++) {
			ImageButton btn = pagerIndicators.get(i);
			if (i == btnIndex) {
				btn.setImageResource(R.drawable.rounded_cell_dark);
			} else {
				btn.setImageResource(R.drawable.rounded_cell_light);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.btnAvailFeaturedItem:
				FeaturedItemPageFragment selectedFeaturedItem = mPagerAdapter.getItem(currentSelectedPage);
				if(selectedFeaturedItem != null){
					FeaturedData featuredData = selectedFeaturedItem.getFeaturedData();
					if(featuredData != null){
						CoreApplication.showToast(CoreApplication.getContext(), "Promo Code : " + featuredData.promoCode, Toast.LENGTH_LONG);
					}
				}
				break;
			default:
				Logger.d(TAG, "Cannot find Clicked View..");
				break;
		}
	}
}

package com.jdnessity.restaurantapp.ui.adapters;

import java.util.ArrayList;

import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab.OrdersRecommendationPageFragment;

/**
 * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
 * sequence.
 */
public class OrderRecommendationsPagerAdapter extends FragmentStatePagerAdapter {
	
	private ArrayList<OrdersRecommendationPageFragment> fragmentItems = new ArrayList<OrdersRecommendationPageFragment>();
	
    public OrderRecommendationsPagerAdapter(FragmentManager fm) {
        super(fm);
    }
    
    public void setItems(ArrayList<OrdersRecommendationPageFragment> fragmentItems){
    	this.fragmentItems = new ArrayList<OrdersRecommendationPageFragment>(fragmentItems);
    }

    @Override
    public OrdersRecommendationPageFragment getItem(int position) {
        return this.fragmentItems.get(position);
    }

    @Override
    public int getCount() {
        return this.fragmentItems.size();
    }
    
    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
    	//FIXME - this prevents crashes when the fragment where this is attached is shown  
    }
}
package com.jdnessity.restaurantapp.ui.adapters;

import java.util.ArrayList;

import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.jdnessity.restaurantapp.ui.fragments.modules.FeaturedTab.FeaturedItemPageFragment;

/**
 * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
 * sequence.
 */
public class FeaturedItemPagerAdapter extends FragmentStatePagerAdapter {
	
	private ArrayList<FeaturedItemPageFragment> fragmentItems = new ArrayList<FeaturedItemPageFragment>();
	
    public FeaturedItemPagerAdapter(FragmentManager fm) {
        super(fm);
    }
    
    public void setItems(ArrayList<FeaturedItemPageFragment> fragmentItems){
    	this.fragmentItems = new ArrayList<FeaturedItemPageFragment>(fragmentItems);
    }

    @Override
    public FeaturedItemPageFragment getItem(int position) {
        return this.fragmentItems.get(position);
    }

    @Override
    public int getCount() {
        return this.fragmentItems.size();
    }

    @Override
    public void restoreState(Parcelable arg0, ClassLoader arg1) {
    	//FIXME - this prevents crashes when the fragment where this is attached is shown  
    }
}
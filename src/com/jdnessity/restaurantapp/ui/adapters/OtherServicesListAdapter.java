package com.jdnessity.restaurantapp.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jdnessity.restaurantapp.CoreApplication;
import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.ui.fragments.modules.OtherServicesTab.objects.OtherServicesData;
import com.jdnessity.restaurantapp.utilities.Logger;

public class OtherServicesListAdapter extends BaseAdapter implements View.OnClickListener{
    
	public final static String TAG = "OtherServicesListAdapter";
	
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<OtherServicesData> otherServicesDataList;
    
    public OtherServicesListAdapter(Context context, ArrayList<OtherServicesData> otherServicesDataList) {
        this.context = context;
        this.otherServicesDataList = new ArrayList<OtherServicesData>(otherServicesDataList);
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
            return otherServicesDataList.size();
    }

    @Override
    public OtherServicesData getItem(int position) {
            return otherServicesDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return otherServicesDataList.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder mViewHolder;
            
            if(convertView == null) {
                convertView = inflater.inflate(R.layout.layout_other_services_list_item, parent, false);
                mViewHolder = new ViewHolder();
                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (ViewHolder) convertView.getTag();
            }
            
            mViewHolder.tvOtherServicesItemDetail = (TextView) convertView.findViewById(R.id.tvOtherServicesItemDetail);
            mViewHolder.ivOtherServicesItem  = (ImageView) convertView.findViewById(R.id.ivOtherServicesItem);
            mViewHolder.tvOtherServicesItemHeader  = (TextView) convertView.findViewById(R.id.tvOtherServicesItemHeader);
            mViewHolder.btnOtherServicesItemTalkToRep = (Button) convertView.findViewById(R.id.btnOtherServicesItemTalkToRep);
            mViewHolder.btnOtherServicesItemTalkToRep.setTag(getItem(position));
            mViewHolder.btnOtherServicesItemTalkToRep.setOnClickListener(this);
            
            setDetails(getItem(position), mViewHolder);
            
            return convertView;
    }
    
    private void setDetails(OtherServicesData otherServicesData, ViewHolder mViewHolder){
    	mViewHolder.ivOtherServicesItem.setImageBitmap(otherServicesData.image);
        mViewHolder.tvOtherServicesItemHeader.setText(otherServicesData.headerText);
        mViewHolder.tvOtherServicesItemDetail.setText(otherServicesData.detailText);
    }
        
    private static class ViewHolder {
    	TextView tvOtherServicesItemDetail, tvOtherServicesItemHeader, btnOtherServicesItemTalkToRep;
    	ImageView ivOtherServicesItem;
    }

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.btnOtherServicesItemTalkToRep:
				Object tag = v.getTag();
				if(tag != null){
					OtherServicesData data = (OtherServicesData) tag;
					CoreApplication.showToast(context, "Service Id: " + data.id, Toast.LENGTH_LONG);
				}else{
					Logger.d(TAG, "Cannot find tag of View...");
				}
				break;
			default:
				break;
		}
	}

}
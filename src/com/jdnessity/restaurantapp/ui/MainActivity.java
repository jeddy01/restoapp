package com.jdnessity.restaurantapp.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jdnessity.restaurantapp.R;
import com.jdnessity.restaurantapp.listeners.OnViewCreatedListener;
import com.jdnessity.restaurantapp.ui.fragments.modules.FeaturedTab.FeaturedTabFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.FeedbackTab.FeedbackTabFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.MenuTab.MenuTabFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.OrdersTab.OrdersTabFragment;
import com.jdnessity.restaurantapp.ui.fragments.modules.OtherServicesTab.OtherServicesTabFragment;
import com.jdnessity.restaurantapp.utilities.Logger;
import com.jdnessity.restaurantapp.widgets.CustomSearchView;

public class MainActivity extends ActionBarActivity {

	public static final String TAG = MainActivity.class.getSimpleName();
	
	//Menu Views
	private TextView featuredTabTextView;
	private TextView feedbackTabTextView;
	private TextView menuTabTextView;
	private TextView ordersTabTextView;
	private TextView otherServicesTabTextView;
	
	//Menu Fragments
	private FeaturedTabFragment featuredTabFragment;
	private FeedbackTabFragment feedbackTabFragment;
	private MenuTabFragment menuTabFragment;
	private OrdersTabFragment ordersTabFragment;
	private OtherServicesTabFragment otherServicesTabFragment;

	//Tracking Tab
	private String currentSelectedTabFragmentTitle = null;
	
	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	//private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		initializeMenuViews();
		
		//Default selected tab upon app launch
		onTabClicked(FeaturedTabFragment.TAG);
	}
	
	private void initializeMenuViews(){
		featuredTabTextView = (TextView) findViewById(R.id.featuredTabTextView);
		featuredTabTextView.setTag(FeaturedTabFragment.TAG);
		
		menuTabTextView = (TextView) findViewById(R.id.menuTabTextView);
		menuTabTextView.setTag(MenuTabFragment.TAG);
		
		ordersTabTextView = (TextView) findViewById(R.id.ordersTabTextView);
		ordersTabTextView.setTag(OrdersTabFragment.TAG);
		
		otherServicesTabTextView = (TextView) findViewById(R.id.otherServicesTabTextView);
		otherServicesTabTextView.setTag(OtherServicesTabFragment.TAG);
		
		feedbackTabTextView = (TextView) findViewById(R.id.feedbackTabTextView);
		feedbackTabTextView.setTag(FeedbackTabFragment.TAG);
	}
	
	/**
	 * Loads the appropriate Fragment
	 * and sets the correct view-state of the menu views.
	 * @param fragmentTitle
	 */
	private void onTabClicked(String fragmentTitle){

		Logger.d(TAG,"Menu clicked: " + fragmentTitle);
		currentSelectedTabFragmentTitle = fragmentTitle;
		
		//Reset all menu views to its default state
		featuredTabTextView.setTextColor(Color.BLACK);
		((LinearLayout)featuredTabTextView.getParent()).setBackgroundColor(android.R.color.transparent);
		
		menuTabTextView.setTextColor(Color.BLACK);
		((LinearLayout)menuTabTextView.getParent()).setBackgroundColor(android.R.color.transparent);
		
		ordersTabTextView.setTextColor(Color.BLACK);
		((LinearLayout)ordersTabTextView.getParent()).setBackgroundColor(android.R.color.transparent);
		
		otherServicesTabTextView.setTextColor(Color.BLACK);
		((LinearLayout)otherServicesTabTextView.getParent()).setBackgroundColor(android.R.color.transparent);
		
		feedbackTabTextView.setTextColor(Color.BLACK);
		((LinearLayout)feedbackTabTextView.getParent()).setBackgroundColor(android.R.color.transparent);
		
		//Set the correct appearance for the tab selected
		if(fragmentTitle != null){
			switch(fragmentTitle){
				case FeaturedTabFragment.TAG:
					featuredTabTextView.setTextColor(Color.WHITE);
					((LinearLayout)featuredTabTextView.getParent()).setBackgroundColor(Color.DKGRAY);
					loadFragment(FeaturedTabFragment.TAG);
					break;
				case MenuTabFragment.TAG:
					menuTabTextView.setTextColor(Color.WHITE);
					((LinearLayout)menuTabTextView.getParent()).setBackgroundColor(Color.DKGRAY);
					loadFragment(MenuTabFragment.TAG);
					break;
				case OrdersTabFragment.TAG:
					ordersTabTextView.setTextColor(Color.WHITE);
					((LinearLayout)ordersTabTextView.getParent()).setBackgroundColor(Color.DKGRAY);
					loadFragment(OrdersTabFragment.TAG);
					break;
				case OtherServicesTabFragment.TAG:
					otherServicesTabTextView.setTextColor(Color.WHITE);
					((LinearLayout)otherServicesTabTextView.getParent()).setBackgroundColor(Color.DKGRAY);
					loadFragment(OtherServicesTabFragment.TAG);
					break;
				case FeedbackTabFragment.TAG:
					feedbackTabTextView.setTextColor(Color.WHITE);
					((LinearLayout)feedbackTabTextView.getParent()).setBackgroundColor(Color.DKGRAY);
					loadFragment(FeedbackTabFragment.TAG);
					break;
				default:
					Logger.d(TAG, "Cannot find TAG of View Clicked..");
					break;
			}
		}
	}
	
	private void loadFragment(String fragmentTitle){
		
		Logger.d(TAG,"Loading fragment: " + fragmentTitle);
		
		Fragment fragmentToLoad = null;
		if(fragmentTitle != null){
			switch(fragmentTitle){
				case FeaturedTabFragment.TAG:
					if(featuredTabFragment == null){
						featuredTabFragment = new FeaturedTabFragment();
						featuredTabFragment.setOnViewCreatedListener(new OnViewCreatedListener() {
							@Override
							public void onViewCreated() {
								/*homeHeaderImageView.setVisibility(View.VISIBLE);
								homeHeaderImageView.setImageResource(R.drawable.netcare_logo);
								titleText.setVisibility(View.INVISIBLE);*/
							}
						});
					}
					fragmentToLoad = featuredTabFragment;
					break;
				case FeedbackTabFragment.TAG:
					if(feedbackTabFragment == null){
						feedbackTabFragment = new FeedbackTabFragment();
						feedbackTabFragment.setOnViewCreatedListener(new OnViewCreatedListener() {
							@Override
							public void onViewCreated() {
								/*homeHeaderImageView.setVisibility(View.VISIBLE);
								homeHeaderImageView.setImageResource(R.drawable.netcare_logo);
								titleText.setVisibility(View.INVISIBLE);*/
							}
						});
					}
					fragmentToLoad = feedbackTabFragment;
					break;
				case MenuTabFragment.TAG:
					if(menuTabFragment == null){
						menuTabFragment = new MenuTabFragment();
						menuTabFragment.setOnViewCreatedListener(new OnViewCreatedListener() {
							@Override
							public void onViewCreated() {
								/*homeHeaderImageView.setVisibility(View.VISIBLE);
								homeHeaderImageView.setImageResource(R.drawable.netcare_logo);
								titleText.setVisibility(View.INVISIBLE);*/
							}
						});
					}
					fragmentToLoad = menuTabFragment;
					break;
				case OrdersTabFragment.TAG:
					if(ordersTabFragment == null){
						ordersTabFragment = new OrdersTabFragment();
						ordersTabFragment.setOnViewCreatedListener(new OnViewCreatedListener() {
							@Override
							public void onViewCreated() {
								/*homeHeaderImageView.setVisibility(View.VISIBLE);
								homeHeaderImageView.setImageResource(R.drawable.netcare_logo);
								titleText.setVisibility(View.INVISIBLE);*/
							}
						});
					}
					fragmentToLoad = ordersTabFragment;
					break;
				case OtherServicesTabFragment.TAG:
					if(otherServicesTabFragment == null){
						otherServicesTabFragment = new OtherServicesTabFragment();
						otherServicesTabFragment.setOnViewCreatedListener(new OnViewCreatedListener() {
							@Override
							public void onViewCreated() {
								/*homeHeaderImageView.setVisibility(View.VISIBLE);
								homeHeaderImageView.setImageResource(R.drawable.netcare_logo);
								titleText.setVisibility(View.INVISIBLE);*/
							}
						});
					}
					fragmentToLoad = otherServicesTabFragment;
					break;
				default:
					Logger.d(TAG, "Unable to find the Fragment Title...");
					break;
			}
			
			if(fragmentToLoad != null){
				getSupportFragmentManager().beginTransaction().replace(R.id.container, fragmentToLoad).commit();
			}
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		final MenuItem searchViewItem = menu.findItem(R.id.menu_main_activity_search_menu);

		final CustomSearchView searchView = (CustomSearchView) MenuItemCompat.getActionView(searchViewItem);
		Logger.d("onCreateOptions: searchView: " + searchView);
		if(searchView != null){
			
			searchView.reset();
			/*int searchImgId = getResources().getIdentifier("android:id/search_button", null, null);
			int searchMagImgId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
			
			ImageView searchViewIcon = (ImageView) searchView.findViewById(searchImgId);
			ImageView searchViewHintIcon = (ImageView) searchView.findViewById(searchMagImgId);
	
			searchViewIcon.setImageResource(R.drawable.ic_action_search);
			searchViewHintIcon.setImageResource(R.drawable.ic_action_search);*/
	
			searchView.setIconifiedByDefault(true);
			searchView.setIconified(true);
			searchView.setOnQueryTextListener(new CustomSearchView.OnQueryTextListener() {
						@Override
						public boolean onQueryTextSubmit(String query) {
							searchView.setIconified(true);
							return false;
						}
	
						@Override
						public boolean onQueryTextChange(String newText) {
							return false;
						}
					});
			searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
						@Override
						public void onFocusChange(View view,
								boolean queryTextFocused) {
							if (!queryTextFocused) {
								if (searchView.getQuery().toString().length() == 0) {
									searchViewItem.collapseActionView();
									searchView.setQuery("", false);
								}
							}
						}
					});
	
			searchView.setQueryHint("Search Menu");
		}
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		int id = item.getItemId();
		switch(id){
		case R.id.menu_main_activity_call_waiter:
			break;
		case R.id.menu_main_activity_search_menu:
			CustomSearchView searchView = (CustomSearchView) MenuItemCompat.getActionView(item);
			if(searchView != null){
				searchView.setIconified(!searchView.isIconified());
			}
			break;
		default:
			Logger.d(MainActivity.class, "Unable to find menu id");
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}


	public void clickHandler(View view) {
		Logger.d(TAG, "Callback!");
		switch (view.getId()) {
			case R.id.featuredTabTextView:
			case R.id.feedbackTabTextView:
			case R.id.menuTabTextView:
			case R.id.ordersTabTextView:
			case R.id.otherServicesTabTextView:
				Object tag = view.getTag();
				if(tag != null){
					String tagString = tag.toString();
					//To avoid reloading of the same tab
					if(!currentSelectedTabFragmentTitle.equals(tagString)){
						onTabClicked(tag.toString());
					}
				}
				break;
			/**
			 * Main Activity
			 */
			default:
				Logger.d(TAG, "View not found");
			break;
		}
	}
	
}
